const orgDetails = [
    {
        name: "Org 1",
        id: 1,
        users: [
            {
                userId: 1,
                userName: "ABC",
                isAdmin: true
            },
            {
                userId: 2,
                userName: "XYZ",
                isAdmin: false
            },
            {
                userId: 3,
                userName: "PQR",
                isAdmin: false
            },
        ]
    },
    {
        name: "Org 2",
        id: 2,
        users: [
            {
                userId: 5,
                userName: "ABC",
                isAdmin: true
            },
            {
                userId: 6,
                userName: "XYZ",
                isAdmin: false
            },
            {
                userId: 7,
                userName: "PQR",
                isAdmin: false
            },
        ]
    },
    {
        name: "Org 3",
        id: 3,
        users: [
            {
                userId: 8,
                userName: "ABC",
                isAdmin: true
            },
            {
                userId: 9,
                userName: "XYZ",
                isAdmin: false
            },
            {
                userId: 10,
                userName: "PQR",
                isAdmin: false
            },
        ]
    },
    {
        name: "Org 4",
        id: 4,
        users: [
            {
                userId: 11,
                userName: "ABC",
                isAdmin: true
            },
            {
                userId: 12,
                userName: "XYZ",
                isAdmin: false
            },
            {
                userId: 13,
                userName: "PQR",
                isAdmin: false
            },
        ]
    }
]

export default orgDetails